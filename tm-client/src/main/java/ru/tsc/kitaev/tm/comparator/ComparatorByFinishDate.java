package ru.tsc.kitaev.tm.comparator;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.api.entity.IHasFinishDate;

import java.util.Comparator;
import java.util.Objects;

@NoArgsConstructor
public final class ComparatorByFinishDate implements Comparator<IHasFinishDate> {

    @NotNull
    private static final ComparatorByFinishDate INSTANCE = new ComparatorByFinishDate();

    @NotNull
    public static ComparatorByFinishDate getInstance() {
        return INSTANCE;
    }

    @Override
    public int compare(@Nullable final IHasFinishDate o1, @Nullable final IHasFinishDate o2) {
        if (o1 == null || o2 == null) return 0;
        return Objects.requireNonNull(o1.getFinishDate()).compareTo(o2.getFinishDate());
    }

}
