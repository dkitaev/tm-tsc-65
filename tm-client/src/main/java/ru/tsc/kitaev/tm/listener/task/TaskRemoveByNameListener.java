package ru.tsc.kitaev.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.kitaev.tm.event.ConsoleEvent;
import ru.tsc.kitaev.tm.util.TerminalUtil;

@Component
public final class TaskRemoveByNameListener extends AbstractTaskListener {

    @NotNull
    @Override
    public String command() {
        return "task-remove-by-name";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove task by name...";
    }

    @Override
    @EventListener(condition = "@taskRemoveByNameListener.command() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("Enter Name");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("[REMOVE PROJECT BY NAME]");
        taskEndpoint.removeTaskByName(sessionService.getSession(), name);
        System.out.println("[OK]");
    }

}
