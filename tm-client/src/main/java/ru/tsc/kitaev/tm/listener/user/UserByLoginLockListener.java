package ru.tsc.kitaev.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.kitaev.tm.event.ConsoleEvent;
import ru.tsc.kitaev.tm.listener.AbstractListener;
import ru.tsc.kitaev.tm.util.TerminalUtil;

@Component
public class UserByLoginLockListener extends AbstractListener {

    @NotNull
    @Override
    public String command() {
        return "user-lock-by-login";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Lock user by login";
    }

    @Override
    @EventListener(condition = "@userByLoginLockListener.command() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[LOCK USER]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();
        adminUserEndpoint.lockUserByLogin(sessionService.getSession(), login);
        System.out.println("[OK]");
    }

}
