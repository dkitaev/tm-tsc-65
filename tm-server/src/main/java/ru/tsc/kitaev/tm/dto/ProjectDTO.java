package ru.tsc.kitaev.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.api.entity.IWBS;
import ru.tsc.kitaev.tm.enumerated.Status;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "projects")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class ProjectDTO extends AbstractOwnerEntityDTO implements IWBS {

    public ProjectDTO(@NotNull String name) {
        this.name = name;
    }

    public ProjectDTO(@NotNull String name, @NotNull String description) {
        this.name = name;
        this.description = description;
    }

    @NotNull
    @Column
    private String name;

    @NotNull
    @Column
    private String description;

    @NotNull
    @Column
    private Status status = Status.NOT_STARTED;

    @Nullable
    @Column(name = "start_date")
    private Date startDate;

    @Nullable
    @Column(name = "finish_date")
    private Date finishDate;

}
