package ru.tsc.kitaev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.tsc.kitaev.tm.api.service.IProjectService;
import ru.tsc.kitaev.tm.client.ProjectRestEndpointClient;
import ru.tsc.kitaev.tm.model.Project;

import java.util.List;

@RestController
@RequestMapping("/api/projects")
public class ProjectEndpointImpl implements ProjectRestEndpointClient {

    @NotNull
    @Autowired
    private IProjectService projectService;

    @Override
    @NotNull
    @PostMapping("/add")
    public Project add(@NotNull @RequestBody final Project project) {
        return projectService.add(project);
    }

    @Override
    @NotNull
    @PutMapping("/save")
    public Project save(@NotNull @RequestBody final Project project) {
        return projectService.update(project);
    }

    @Override
    @Nullable
    @GetMapping("/findAll")
    public List<Project> findAll() {
        return projectService.findAll();
    }

    @Override
    @Nullable
    @GetMapping("/findById/{id}")
    public Project findById(@NotNull @PathVariable(value = "id") final String id) {
        return projectService.findById(id);
    }

    @Override
    @GetMapping("/existsById/{id}")
    public boolean existsById(@Nullable @PathVariable(value = "id") final String id) {
        return projectService.existsById(id);
    }

    @Override
    @GetMapping("/count")
    public int getSize() {
        return projectService.getSize();
    }

    @Override
    @DeleteMapping("/clear")
    public void clear() {
        projectService.clear();
    }

    @Override
    @DeleteMapping("/delete")
    public void delete(@NotNull @RequestBody final Project project) {
        projectService.delete(project);
    }

    @Override
    @DeleteMapping("/deleteById/{id}")
    public void deleteById(@NotNull @PathVariable(value = "id") final String id) {
        projectService.deleteById(id);
    }

}
